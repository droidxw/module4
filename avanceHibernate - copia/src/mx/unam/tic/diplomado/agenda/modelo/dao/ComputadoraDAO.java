package mx.unam.tic.diplomado.agenda.modelo.dao;

import java.util.List;

import mx.unam.tic.diplomado.agenda.modelo.entidades.Cliente;
import mx.unam.tic.diplomado.agenda.modelo.entidades.Cobro;
import mx.unam.tic.diplomado.agenda.modelo.entidades.PrecioComputadora;

public interface ComputadoraDAO {

	Cliente cargaClientePorId(Integer id);

	List<Cliente> cargaClientes();

	void guardaContacto(Cliente cliente);

	void actualizaContacto(Cliente contacto);

	void eliminaContacto(Cliente contacto);
	
	Boolean guardaTipoContacto(PrecioComputadora tipoContacto);
	
	Boolean guardaMedioContacto(Cobro medioContacto);
}
