package mx.unam.tic.diplomado.agenda.modelo.entidades;

public class PrecioComputadora {
	
	private Integer id;
	private float precioComputadora;
	private Cobro cobro;
	

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public float getPrecioComputadora() {
		return precioComputadora;
	}
	public void setPrecioComputadora(float precioComputadora) {
		this.precioComputadora = precioComputadora;
	}
	public Cobro getCobro() {
		return cobro;
	}
	public void setCobro(Cobro cobro) {
		this.cobro = cobro;
	}


}
