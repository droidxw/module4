package mx.unam.tic.diplomado.agenda.modelo.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import mx.unam.tic.diplomado.agenda.modelo.entidades.Cobro;
import mx.unam.tic.diplomado.agenda.modelo.entidades.PrecioComputadora;
import mx.unam.tic.diplomado.agenda.modelo.hibernate.HibernateUtil;
//ontencion de catalogos o de un valor por medio de un objeto que representa una tabla
public class CatalogosDAOImpl implements CatalogosDAO {
	//**singleton (nos permite instanciar un objeto n veces
	//**y que esta sea siempre la misma)
	/////En este caso unicamente obtener datos
	//variable privada y estatica (mantiene valor al inicializar)	
	private static CatalogosDAOImpl instance;
///constructor privado
	private CatalogosDAOImpl() {

	}
//metodo publica y estatica
	public static CatalogosDAOImpl getInstance() {
		if (instance == null) {
			instance = new CatalogosDAOImpl();
		}
		return instance;
	}
	//**singleton(un solo objeto de esa clase)
	@Override
	public List<PrecioComputadora> cargaTiposContacto() {
		List<PrecioComputadora> catalogo = null;
		try {
			//se obtiene la sesion
			Session session = HibernateUtil.getSessionFactory().openSession();

			// se inicia una transaccion
			session.beginTransaction();
			//HQL filtrado con alias y estatus activo (borrado logico)
			catalogo = session.createQuery("SELECT t FROM TipoContacto t WHERE t.estatus = 'ACTIVO' ").list();
			//con get(podría ser también unique de HQL) se obtiene info y se realiza el commit
			session.getTransaction().commit();
			// se cierra la session hibernate
			session.close();
		} catch (Exception e) {
			e.printStackTrace();
			StandardServiceRegistryBuilder.destroy(HibernateUtil.getRegistry());
		}

		return catalogo;
	}

	@Override
	public PrecioComputadora cargaTipoContactoPorId(Integer id) {
		PrecioComputadora tipoContacto = null;
		try {

			Session session = HibernateUtil.getSessionFactory().openSession();

			// se inicia una transaccion
			session.beginTransaction();
			//clase a consultar y valor en el get			
			tipoContacto = session.get(PrecioComputadora.class, id);
			// se realiza el commit
			session.getTransaction().commit();
			// se cierra la session hibernate
			session.close();
		} catch (Exception e) {
			e.printStackTrace();
			StandardServiceRegistryBuilder.destroy(HibernateUtil.getRegistry());
		}
		return tipoContacto;
	}

	@Override
	public List<Cobro> cargaMediosContacto() {
		List<Cobro> catalogo = null;
		try {

			Session session = HibernateUtil.getSessionFactory().openSession();

			// se inicia una transaccion
			session.beginTransaction();
			catalogo = session.createQuery("SELECT t FROM MedioContacto t WHERE t.estatus = 'ACTIVO' ").list();
			// se realiza el commit
			session.getTransaction().commit();
			// se cierra la session hibernate
			session.close();
		} catch (Exception e) {
			e.printStackTrace();
			StandardServiceRegistryBuilder.destroy(HibernateUtil.getRegistry());
		}

		return catalogo;
	}

	@Override
	public Cobro cargaMedioContactoPorId(Integer id) {
		Cobro medioContacto = null;
		try {

			Session session = HibernateUtil.getSessionFactory().openSession();

			// se inicia una transaccion
			session.beginTransaction();
			medioContacto = session.get(Cobro.class, id);
			// se realiza el commit
			session.getTransaction().commit();
			// se cierra la session hibernate
			session.close();
		} catch (Exception e) {
			e.printStackTrace();
			StandardServiceRegistryBuilder.destroy(HibernateUtil.getRegistry());
		}
		return medioContacto;
	}
	@Override
	public Boolean guardaTipoContacto(PrecioComputadora tipoContacto) {
		Boolean save=Boolean.FALSE;
		try {

			Session session = HibernateUtil.getSessionFactory().openSession();

			// se inicia una transaccion
			session.beginTransaction();
			session.save(tipoContacto);
			save =tipoContacto.getId()!=null?Boolean.TRUE:Boolean.FALSE;
			// se realiza el commit
			session.getTransaction().commit();
			// se cierra la session hibernate
			session.close();
			save=true;
		} catch (Exception e) {
			e.printStackTrace();
			StandardServiceRegistryBuilder.destroy(HibernateUtil.getRegistry());
		}
		return save;
	}
	
	@Override
	public Boolean guardaMedioContacto(Cobro medioContacto) {
		boolean save=false;
		try {

			Session session = HibernateUtil.getSessionFactory().openSession();

			// se inicia una transaccion
			session.beginTransaction();
			session.save(medioContacto);
			save =medioContacto.getId()!=null?true:false;
			// se realiza el commit
			session.getTransaction().commit();
			// se cierra la session hibernate
			session.close();
			save=true;
		} catch (Exception e) {
			e.printStackTrace();
			StandardServiceRegistryBuilder.destroy(HibernateUtil.getRegistry());
		}
		return save;
	}
}
