package mx.unam.tic.diplomado.agenda.modelo.entidades;

import java.util.Date;
import java.util.Set;

public class Cliente {
	

	private Integer id;
	private String nombreCompleto;
	private String fechaNacimiento;
	private String direccion;
	private String telefono;
	private String RFC;
	private Cobro cobro;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getRFC() {
		return RFC;
	}
	public void setRFC(String rFC) {
		RFC = rFC;
	}
	public Cobro getCobro() {
		return cobro;
	}
	public void setCobro(Cobro cobro) {
		this.cobro = cobro;
	}
	
	@Override
	public String toString() {
		return "Cliente [id=" + id + ", nombreCompleto=" + nombreCompleto + ", fechaNacimiento=" + fechaNacimiento
				+ ", direccion=" + direccion + ", telefono=" + telefono + ", RFC=" + RFC + "]";
	}


}
