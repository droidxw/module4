package mx.unam.tic.diplomado.agenda.modelo.entidades;

public class TipoComputadora {
	

	private Integer id;
	private float escritorioPrecio;
	private float portatilPrecio;
	private float servidorPrecio;	
	private String fabricante;
	private String clave;
	private Integer existencias;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public float getEscritorioPrecio() {
		return escritorioPrecio;
	}
	public void setEscritorioPrecio(float escritorioPrecio) {
		this.escritorioPrecio = escritorioPrecio;
	}
	public float getPortatilPrecio() {
		return portatilPrecio;
	}
	public void setPortatilPrecio(float portatilPrecio) {
		this.portatilPrecio = portatilPrecio;
	}
	public float getServidorPrecio() {
		return servidorPrecio;
	}
	public void setServidorPrecio(float servidorPrecio) {
		this.servidorPrecio = servidorPrecio;
	}
	public String getFabricante() {
		return fabricante;
	}
	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public Integer getExistencias() {
		return existencias;
	}
	public void setExistencias(Integer existencias) {
		this.existencias = existencias;
	}
	
	
	
	
	


}
