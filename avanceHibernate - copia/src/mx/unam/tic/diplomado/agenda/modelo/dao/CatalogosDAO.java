package mx.unam.tic.diplomado.agenda.modelo.dao;

import java.util.List;

import mx.unam.tic.diplomado.agenda.modelo.entidades.Cobro;
import mx.unam.tic.diplomado.agenda.modelo.entidades.PrecioComputadora;

public interface CatalogosDAO {
	
	List<PrecioComputadora> cargaTiposContacto();
			
	List<Cobro> cargaMediosContacto();
	
	PrecioComputadora cargaTipoContactoPorId(Integer id);
	
	Cobro cargaMedioContactoPorId(Integer id);
	
	Boolean guardaTipoContacto(PrecioComputadora tipoContacto);
	
	Boolean guardaMedioContacto(Cobro medioContacto);

}
