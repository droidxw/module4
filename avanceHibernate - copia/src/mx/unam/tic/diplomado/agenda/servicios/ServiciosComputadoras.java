package mx.unam.tic.diplomado.agenda.servicios;

import java.util.List;

import mx.unam.tic.diplomado.agenda.modelo.entidades.Cliente;

public interface ServiciosComputadoras {

	Cliente cargaClientePorId(Integer id);

	List<Cliente> cargaClientes();

	void guardaContacto(Cliente cliente);

	void actualizaContacto(Cliente contacto);

	void eliminaContacto(Cliente contacto);

}
