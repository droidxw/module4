package mx.unam.tic.diplomado.agenda.servicios;

import java.util.List;

import mx.unam.tic.diplomado.agenda.modelo.dao.CatalogosDAO;
import mx.unam.tic.diplomado.agenda.modelo.dao.CatalogosDAOImpl;
import mx.unam.tic.diplomado.agenda.modelo.entidades.Cobro;
import mx.unam.tic.diplomado.agenda.modelo.entidades.PrecioComputadora;

public class ServiciosCatalogosImpl implements ServiciosCatalogos {
	
	private static ServiciosCatalogosImpl instance;
	
	private ServiciosCatalogosImpl() {
		
	}
	
	public static ServiciosCatalogosImpl getInstance() {
		if (instance == null) {
			instance = new ServiciosCatalogosImpl();
		}
		return instance;
	}
	
	@Override
	public List<PrecioComputadora> cargaTiposContacto() {
		CatalogosDAO catalogosDAO = CatalogosDAOImpl.getInstance();
		return catalogosDAO.cargaTiposContacto();
	}
	
	@Override
	public PrecioComputadora cargaTipoContactoPorId(Integer id) {
		CatalogosDAO catalogosDAO = CatalogosDAOImpl.getInstance();
		return catalogosDAO.cargaTipoContactoPorId(id);
	}
	
	@Override
	public List<Cobro> cargaMediosContacto() {
		CatalogosDAO catalogosDAO = CatalogosDAOImpl.getInstance();
		return catalogosDAO.cargaMediosContacto();
	}

	@Override
	public Cobro cargaMedioContactoPorId(Integer id) {
		CatalogosDAO catalogosDAO = CatalogosDAOImpl.getInstance();
		return catalogosDAO.cargaMedioContactoPorId(id);
	}

	@Override
	public Boolean guardaTipoContacto(PrecioComputadora tipoContacto) {
		CatalogosDAO catalogosDAO = CatalogosDAOImpl.getInstance();
		return catalogosDAO.guardaTipoContacto(tipoContacto);
	}

	@Override
	public Boolean guardaMedioContacto(Cobro medioContacto) {
		CatalogosDAO catalogosDAO = CatalogosDAOImpl.getInstance();
		return catalogosDAO.guardaMedioContacto(medioContacto);
	}

}
