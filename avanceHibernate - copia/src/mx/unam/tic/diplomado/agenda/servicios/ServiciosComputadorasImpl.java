package mx.unam.tic.diplomado.agenda.servicios;

import java.util.List;

import mx.unam.tic.diplomado.agenda.modelo.dao.ComputadoraDAO;
import mx.unam.tic.diplomado.agenda.modelo.dao.ComputadoraDAOImpl;
import mx.unam.tic.diplomado.agenda.modelo.entidades.Cliente;

public class ServiciosComputadorasImpl implements ServiciosComputadoras {
	
	private static ServiciosComputadorasImpl instance;
	
	private ServiciosComputadorasImpl() {}
	
	public static ServiciosComputadorasImpl getInstance() {
		if (instance == null) {
			instance = new ServiciosComputadorasImpl();
		}
		return instance;
	}

	@Override
	public Cliente cargaClientePorId(Integer id) {
		ComputadoraDAO contactoDAO = ComputadoraDAOImpl.getInstance();
		return contactoDAO.cargaClientePorId(id);
	}

	@Override
	public List<Cliente> cargaClientes() {
		ComputadoraDAO contactoDAO = ComputadoraDAOImpl.getInstance();
		return contactoDAO.cargaClientes();
	}

	@Override
	public void guardaContacto(Cliente cliente) {
		ComputadoraDAO computadoraDAO = ComputadoraDAOImpl.getInstance();
		computadoraDAO.guardaContacto(cliente);
	}

	@Override
	public void actualizaContacto(Cliente contacto) {
		ComputadoraDAO contactoDAO = ComputadoraDAOImpl.getInstance();
		contactoDAO.actualizaContacto(contacto);
	}

	@Override
	public void eliminaContacto(Cliente contacto) {
		ComputadoraDAO contactoDAO = ComputadoraDAOImpl.getInstance();
		contactoDAO.eliminaContacto(contacto);
	}

}
