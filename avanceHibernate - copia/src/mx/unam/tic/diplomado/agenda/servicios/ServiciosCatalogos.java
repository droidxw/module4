package mx.unam.tic.diplomado.agenda.servicios;

import java.util.List;

import mx.unam.tic.diplomado.agenda.modelo.entidades.Cobro;
import mx.unam.tic.diplomado.agenda.modelo.entidades.PrecioComputadora;

public interface ServiciosCatalogos {	
	
	List<PrecioComputadora> cargaTiposContacto();
	
	List<Cobro> cargaMediosContacto();
	
	PrecioComputadora cargaTipoContactoPorId(Integer id);

	Cobro cargaMedioContactoPorId(Integer id);
	
	Boolean guardaTipoContacto(PrecioComputadora tipoContacto);
	
	Boolean guardaMedioContacto(Cobro medioContacto);
}
