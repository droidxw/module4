
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import mx.unam.tic.diplomado.agenda.modelo.entidades.Cliente;
import mx.unam.tic.diplomado.agenda.modelo.entidades.TipoComputadora;
import mx.unam.tic.diplomado.agenda.modelo.entidades.Cobro;
import mx.unam.tic.diplomado.agenda.modelo.entidades.PrecioComputadora;
import mx.unam.tic.diplomado.agenda.modelo.hibernate.HibernateUtil;
import mx.unam.tic.diplomado.agenda.servicios.ServiciosCatalogos;
import mx.unam.tic.diplomado.agenda.servicios.ServiciosCatalogosImpl;
import mx.unam.tic.diplomado.agenda.servicios.ServiciosComputadoras;
import mx.unam.tic.diplomado.agenda.servicios.ServiciosComputadorasImpl;

public class MapeoAPP {

	///Inicializa servicios
	ServiciosCatalogos serviciosCatalogos = ServiciosCatalogosImpl.getInstance();
	static ServiciosComputadoras serviciosComputadoras = ServiciosComputadorasImpl.getInstance();
	
	public void cargaMenu(){
		Scanner sn = new Scanner(System.in);
        boolean salir = false;
        int opcion; //Guardaremos la opcion del usuario
        do{
            System.out.println("1. Crear registro");
            System.out.println("2. Consultar registros");
            System.out.println("3. Consultar registro por Id");            
//            System.out.println("4. Agregar Tipo de Contacto");
//            System.out.println("5. Agregar Medio de Contacto");
//            System.out.println("6. Consultar catalogos");
            System.out.println("4. Salir");
            try {
                System.out.println("Escribe una de las opciones");
                opcion = sn.nextInt();
                switch (opcion) {
                    case 1:
                        System.out.println("Inicio de creacion registro...");
                        creaRegistro();
                        break;
                    case 2:
                        System.out.println("Cargando registros...");
                        consultaRegistros();
                        break;
                    case 3:                        
                        consultaRegistrosId();
                        break; 
//                    case 4:                        
//                    	creaTipoContacto() ;
//                        break;  
//                    case 5:                        
//                    	creaMedioContacto() ;
//                        break;
//                    case 6:                        
//                    	muestraCatalogos();
//                        break; 
                    case 7:
                    	salir = true;
                        break;
                   
                    default:
                        System.out.println("Solo numeros entre 1 y 3");
                }
            } catch (InputMismatchException e) {
                System.out.println("Debes insertar un numero");
                sn.next();
            }
        }
        while (!salir) ;
	}
	
//	//CONSULTA todos los contactos
	public void consultaRegistros() {
		
		List<Cliente> clientes = serviciosComputadoras.cargaClientes();
		//Muestra info cliente
		for (Cliente clienteT : clientes) {
			System.out.println("*El coclientes:" + clienteT.getId());
//			System.out.println("*El tipo contacto es:" + contactoT.getTipoContacto().getNombre());
		}		
	}	
	
	//CONSULTA POR ID
	public void consultaRegistrosId() {
		System.out.println("Introduce el valor a consultar");
		Scanner sn = new Scanner(System.in);
		int id = sn.nextInt();
		Cliente cliente1 = serviciosComputadoras.cargaClientePorId(id);
		System.out.println("Id"+cliente1.getId());
		System.out.println("*El cliente es:" + cliente1.toString());
//		System.out.println("*El tipo contacto es:" + contacto1.getTipoContacto().getNombre());
	}	
	
	//Definicion nuevo contacto			
	public void  creaRegistro()  {	
		SimpleDateFormat simpleDateFormat ;
	Scanner sn = new Scanner(System.in);
	
	System.out.println("Introduce el nombre completo");
	String nombre = sn.nextLine();
	System.out.println("fecha nacimiento (yyyy-MM-dd)");	
	String fecha = sn.nextLine();	
	System.out.println("Introduce la direccion");
	String direccion = sn.nextLine();
	System.out.println("Introduce el telefono");
	String telefono = sn.nextLine();
	System.out.println("Introduce el RFC");
	String rfc = sn.nextLine();
	
//	simpleDateFormat= new SimpleDateFormat();
//	Date fechanac = simpleDateFormat.format(new Date());
//	Date fechanac = simpleDateFormat.parse(fecha);
	
//	//int
//	int edad = sn.nextInt();
//	System.out.println("Introduce el id de tipo de cliente");
//	//valores del 1 al 3
//	int tipoCId = sn.nextInt();	
//	//valores del 1 al 3
//	
//	System.out.println("Introduce el id de medio de cliente");
//	int medioId = sn.nextInt();
//	
//	System.out.println("Introduce el numero telefonico");
//	//# telefonico
//	String medioContactoV = sn.nextLine();

	Cliente cliente = new Cliente();		
	cliente.setNombreCompleto(nombre);
	cliente.setFechaNacimiento(fecha);
	cliente.setDireccion(direccion);
	cliente.setTelefono(telefono);	
	cliente.setRFC(rfc);	
	

//	c_tipocontacto	1Fam 2Esc 3Lab		
//	TipoContacto tipocontacto = serviciosCatalogos.cargaTipoContactoPorId(tipoCId);	
//	contacto.setTipoContacto(tipocontacto);
//	
//	
////***********************************************************Contacto
////	c_mediocontacto 1casa 2trabajo 3celular
//	MedioContacto medioContacto = serviciosCatalogos.cargaMedioContactoPorId(medioId);
//
//	//Definicion nuevo medio
//	ContactoMedio contactoMedio = new ContactoMedio();
////	atrib t_contactomedio		
//	contactoMedio.setValor(medioContactoV);
//	contactoMedio.setMedioContacto(medioContacto);
////	atrib t_contacto		
//	contactoMedio.setContacto(contacto);
//	
//	//Lista t_contacto
//	Set<ContactoMedio> contactosMedios = new HashSet<ContactoMedio>();
//	contactosMedios.add(contactoMedio);
//	
//	contacto.setContactosMedios(contactosMedios);		
	//guarda info completa en t_contacto
	serviciosComputadoras.guardaContacto(cliente);
	
}		
//	
//	public void  creaTipoContacto() {	
//		
//		Scanner sn = new Scanner(System.in);
//		System.out.println("Introduce el id");
//		int id= sn.nextInt();
//		System.out.println("Introduce el nombre");
//		String nombre= sn.next();
//		System.out.println("Introduce el estatus");
//		String estatus= sn.next();		
//		
//		TipoContacto tipocontacto = new TipoContacto();		
//		tipocontacto.setId(id);
//		tipocontacto.setNombre(nombre);
//		tipocontacto.setEstatus(estatus);
//		serviciosCatalogos.guardaTipoContacto(tipocontacto);		
//	}	
//	
//	
//public void  creaMedioContacto() {	
//		
//		Scanner sn = new Scanner(System.in);
//		System.out.println("Introduce el id");
//		int id= sn.nextInt();
//		System.out.println("Introduce el nombre");
//		String nombre= sn.next();
//		System.out.println("Introduce el estatus");
//		String estatus= sn.next();		
//		
//		MedioContacto mediocontacto = new MedioContacto();		
//		mediocontacto .setId(id);
//		mediocontacto .setNombre(nombre);
//		mediocontacto .setEstatus(estatus);
//		serviciosCatalogos.guardaMedioContacto (mediocontacto);			
//	}	
//	
//
//public void  muestraCatalogos() {	
//	Scanner sn = new Scanner(System.in);
//	System.out.println("Introduce el nombre(s)");
////*********Muestra Catalogos*********
////*******************tipo*******************		
//			
//		List<TipoContacto> tiposContacto = serviciosCatalogos.cargaTiposContacto();
//		for (TipoContacto tipoContacto : tiposContacto) {
//			System.out.println("-El tipo contacto es:" + tipoContacto.getNombre());
//		}
//
//		TipoContacto familiar = serviciosCatalogos.cargaTipoContactoPorId(1);
//		System.out.println("-El contacto familiar===" + familiar);
//		
////*******************medio*******************	
//		List<MedioContacto> mediosContacto = serviciosCatalogos.cargaMediosContacto();
//		for (MedioContacto medioContacto : mediosContacto) {
//			System.out.println("-El medio contacto es:" + medioContacto.getNombre());
//		}
//
//		MedioContacto casa = serviciosCatalogos.cargaMedioContactoPorId(2);
//		System.out.println("-El medio casa===" + casa);
//		
//		
		
//**************************************		
//*********FinMuestra Catalogos*********
	
//}	

	public static void main(String[] args)  {
		
		HibernateUtil.init();
		MapeoAPP catalogos = new MapeoAPP();
		catalogos.cargaMenu();		

	}

}
