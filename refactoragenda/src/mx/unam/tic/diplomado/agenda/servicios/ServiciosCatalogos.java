package mx.unam.tic.diplomado.agenda.servicios;

import java.util.List;

import mx.unam.tic.diplomado.agenda.modelo.entidades.MedioContacto;
import mx.unam.tic.diplomado.agenda.modelo.entidades.TipoContacto;

public interface ServiciosCatalogos {	
	
	List<TipoContacto> cargaTiposContacto();
	
	List<MedioContacto> cargaMediosContacto();
	
	TipoContacto cargaTipoContactoPorId(Integer id);

	MedioContacto cargaMedioContactoPorId(Integer id);
	
	Boolean guardaTipoContacto(TipoContacto tipoContacto);
	
	Boolean guardaMedioContacto(MedioContacto medioContacto);
}
