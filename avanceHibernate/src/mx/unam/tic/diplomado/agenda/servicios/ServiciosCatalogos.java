package mx.unam.tic.diplomado.agenda.servicios;

import java.util.List;

import mx.unam.tic.diplomado.agenda.modelo.entidades.Cobro;
import mx.unam.tic.diplomado.agenda.modelo.entidades.Precio;

public interface ServiciosCatalogos {	
	
	List<Precio> cargaTiposContacto();
	
	List<Cobro> cargaMediosContacto();
	
	Precio cargaTipoContactoPorId(Integer id);

	Cobro cargaMedioContactoPorId(Integer id);
	
	Boolean guardaTipoContacto(Precio tipoContacto);
	
	Boolean guardaMedioContacto(Cobro medioContacto);
}
