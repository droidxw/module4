package mx.unam.tic.diplomado.agenda.servicios;

import java.util.List;

import mx.unam.tic.diplomado.agenda.modelo.entidades.Cliente;
import mx.unam.tic.diplomado.agenda.modelo.entidades.Cobro;
import mx.unam.tic.diplomado.agenda.modelo.entidades.Precio;

public interface ServiciosComputadoras {

	Cliente cargaClientePorId(Integer id);

	List<Cliente> cargaClientes();

	void guardaContacto(Cliente cliente);

	void actualizaContacto(Cliente contacto);

	void eliminaContacto(Integer contacto);

	List<Precio> cargaPrecio(Integer idTipoContacto);
	
	Cobro cargaNota (Integer idCliente);

}
