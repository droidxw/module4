package mx.unam.tic.diplomado.agenda.modelo.entidades;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "t_cobro")
@NamedQueries({
//		@NamedQuery(name = "contactosTotal", query = "FROM Cobro")
		 @NamedQuery(name = "cobroTotal", query = "SELECT c FROM Cobro c WHERE c.id = :id")
		})
public class Cobro {
	
	@Id
	@Column(name = "id_cobro")
	@GeneratedValue(strategy = GenerationType.IDENTITY)//protocolo	
	private Integer id;
	@Column(name = "cob_cantidad_articulos")
	private Integer cantidad;
	
	
	@Column(name = "cob_fecha")
	private Date fecha;
	@Column(name = "cob_monto_total")
	private float cuenta;
	@Column(name = "cob_metodo_pago")
	private String metodo;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_precio")
	private Precio precio;
	@ManyToOne(targetEntity = Cliente.class, fetch = FetchType.LAZY, optional = false) //indica el tipo de clase
	@JoinColumn(name = "id_cliente", nullable = false)//indicar la columna de la relacion de B
	private Cliente cliente;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public float getCuenta() {
		return cuenta;
	}
	public void setCuenta(float cuenta) {
		this.cuenta = cuenta;
	}
	public String getMetodo() {
		return metodo;
	}
	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}
	
	
	public Precio getPrecio() {
		return precio;
	}
	public void setPrecio(Precio precio) {
		this.precio = precio;
	}
	

	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	@Override
	public String toString() {
		return "Cobro [id=" + id + ", cantidad=" + cantidad + ", fecha=" + fecha + ", cuenta=" + cuenta + ", metodo="
				+ metodo + "]";
	}

	
	
	

}
