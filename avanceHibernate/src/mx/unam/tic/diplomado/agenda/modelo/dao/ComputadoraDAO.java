package mx.unam.tic.diplomado.agenda.modelo.dao;

import java.util.List;

import mx.unam.tic.diplomado.agenda.modelo.entidades.Cliente;
import mx.unam.tic.diplomado.agenda.modelo.entidades.Cobro;
import mx.unam.tic.diplomado.agenda.modelo.entidades.Precio;

public interface ComputadoraDAO {

	Cliente cargaClientePorId(Integer id);

	List<Cliente> cargaClientes();
	
	 List<Precio> cargaPrecio(Integer idTipoEquipo) ;

	 Cobro cargaNota (Integer idCliente);

	void guardaContacto(Cliente cliente);

	void actualizaContacto(Cliente contacto);

	void eliminaContacto(Integer contacto);
	
	Boolean guardaTipoContacto(Precio tipoContacto);
	
	Boolean guardaMedioContacto(Cobro medioContacto);
}
