package mx.unam.tic.diplomado.agenda.modelo.entidades;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "t_cliente")
@NamedQueries(
		{@NamedQuery(name = "contactosAll", query = "FROM Cliente"),
		 @NamedQuery(name = "contactoById", query = "SELECT c FROM Cliente c WHERE c.id = :id"),
//		 @NamedQuery(name = "contactosTotal", query = "FROM Cobro")
		})
public class Cliente {
	
	@Id
	@Column(name = "id_cliente")
	@GeneratedValue(strategy = GenerationType.IDENTITY)//protocolo
	private Integer id;
	@Column(name = "clien_nombre_completo", length = 50)
	private String nombreCompleto;
	@Column(name = "clien_fecha_nac", length = 50)
	private String fechaNacimiento;
	@Column(name = "clien_direccion", length = 50)
	private String direccion;
	@Column(name = "clien_telefono", length = 50)
	private String telefono;
	@Column(name = "clien_rfc", length = 50)
	private String RFC;
	@OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)//subconsultas de objetos relacionados se puede con FetchType.EAGER pero no es optimo trae todas las
	private Set<Cobro> cobro;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getRFC() {
		return RFC;
	}
	public void setRFC(String rfc) {
		RFC = rfc;
	}

	
	@Override
	public String toString() {
		return "Cliente [id=" + id + ", nombreCompleto=" + nombreCompleto + ", fechaNacimiento=" + fechaNacimiento
				+ ", direccion=" + direccion + ", telefono=" + telefono + ", RFC=" + RFC + "]";
	}


}
