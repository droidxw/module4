package mx.unam.tic.diplomado.agenda.modelo.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import mx.unam.tic.diplomado.agenda.modelo.entidades.Cliente;
import mx.unam.tic.diplomado.agenda.modelo.entidades.Cobro;
import mx.unam.tic.diplomado.agenda.modelo.entidades.Precio;
import mx.unam.tic.diplomado.agenda.modelo.hibernate.HibernateUtil;

public class ComputadoraDAOImpl implements ComputadoraDAO {

	private static ComputadoraDAOImpl instance;

	private ComputadoraDAOImpl() {
	}

	public static ComputadoraDAOImpl getInstance() {
		if (instance == null) {
			instance = new ComputadoraDAOImpl();
		}
		return instance;
	}

	@Override
	public Cliente cargaClientePorId(Integer id) {
		Cliente contacto = null;
		try {
//contacto.getId(2);
			Session session = HibernateUtil.getSessionFactory().openSession();

			// se inicia una transaccion
			session.beginTransaction();
			contacto = session.get(Cliente.class, id);
			// se realiza el commit
			session.getTransaction().commit();
			// se cierra la session hibernate
			session.close();
		} catch (Exception e) {
			e.printStackTrace();
			StandardServiceRegistryBuilder.destroy(HibernateUtil.getRegistry());
		}
		return contacto;
	}

	@Override
	public List<Cliente> cargaClientes() {
		List<Cliente> catalogo = null;
		try {

			Session session = HibernateUtil.getSessionFactory().openSession();

			// se inicia una transaccion
			session.beginTransaction();
			//agregar status para mapear y filtrar sobre datos vigentes
			catalogo = session.createNamedQuery("contactosAll", Cliente.class).list();
			System.out.println("carga"+ catalogo);
			// se realiza el commit
			session.getTransaction().commit();
			// se cierra la session hibernate
			session.close();
		} catch (Exception e) {
			e.printStackTrace();
			StandardServiceRegistryBuilder.destroy(HibernateUtil.getRegistry());
		}

		return catalogo;
	}
	
	
	@Override
	public List<Precio> cargaPrecio(Integer idTipoEquipo) {
		List<Precio> contactos = null;
		try {

			Session session = HibernateUtil.getSessionFactory().openSession();

			// se inicia una transaccion
			session.beginTransaction();
			contactos = session
//					.createNamedQuery("contactosByTipo", Contacto.class)
					.createNamedQuery("precioEquipo")
					.setParameter("idTipoEquipo", idTipoEquipo).getResultList();//asignamos valor al parametro idTipoContacto del namedquery
			// se realiza el commit
			session.getTransaction().commit();
			// se cierra la session hibernate
			session.close();
		} catch (Exception e) {
			e.printStackTrace();
			StandardServiceRegistryBuilder.destroy(HibernateUtil.getRegistry());
		}
		return contactos;
	}

	@Override
	public void guardaContacto(Cliente cliente) {
		try {

			Session session = HibernateUtil.getSessionFactory().openSession();

			// se inicia una transaccion
			session.beginTransaction();
			session.save(cliente);
			// se realiza el commit
			session.getTransaction().commit();
			// se cierra la session hibernate
			session.close();
		} catch (Exception e) {
			e.printStackTrace();
			StandardServiceRegistryBuilder.destroy(HibernateUtil.getRegistry());
		}
	}

	@Override
	public void actualizaContacto(Cliente contacto) {
		Cliente contacto1 = contacto;
		try {

			Session session = HibernateUtil.getSessionFactory().openSession();

			// se inicia una transaccion
			session.beginTransaction();
			//metodo saveorupdate (hibernate) de guardado/actualizacion dependiendo del caso  
			session.update(contacto);
			// se realiza el commit
			session.getTransaction().commit();
			// se cierra la session hibernate
			session.close();
		} catch (Exception e) {
			e.printStackTrace();
			StandardServiceRegistryBuilder.destroy(HibernateUtil.getRegistry());
		}
	}

	@Override
	public void eliminaContacto(Integer contacto) {
		try {

			Session session = HibernateUtil.getSessionFactory().openSession();

			// se inicia una transaccion
			session.beginTransaction();
			
			if (contacto!=null) {
				session.remove(contacto);
			}
			// se realiza el commit
			session.getTransaction().commit();
			// se cierra la session hibernate
			session.close();
		} catch (Exception e) {
			e.printStackTrace();
			StandardServiceRegistryBuilder.destroy(HibernateUtil.getRegistry());
		}
	}
	
	@Override
	public Boolean guardaTipoContacto(Precio tipoContacto) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Boolean guardaMedioContacto(Cobro medioContacto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public  Cobro cargaNota (Integer idCliente) {
		Object datos = null;
		try {

			Session session = HibernateUtil.getSessionFactory().openSession();

			// se inicia una transaccion
			session.beginTransaction();
//			datos= session.createQuery("SELECT c FROM Cobro c WHERE c.id = :id").list();
			datos=session.get(Cobro.class, idCliente);
//			datos=session.get(Cliente.class,idCliente).getNombreCompleto();
//--			datos=session.createNamedQuery("contactosTotal", Cobro.class).list();
//			contacto = (Contacto)session
//					.createNamedQuery("contactoById", Contacto.class)
//					.setParameter("idCliente", idCliente).getSingleResult();
			
			
			// se realiza el commit
			session.getTransaction().commit();
			// se cierra la session hibernate
			session.close();
		} catch (Exception e) {
			e.printStackTrace();
			StandardServiceRegistryBuilder.destroy(HibernateUtil.getRegistry());
		}
		return  (Cobro) datos;
	}

}
