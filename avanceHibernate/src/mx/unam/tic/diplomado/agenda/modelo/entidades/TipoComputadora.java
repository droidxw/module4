package mx.unam.tic.diplomado.agenda.modelo.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_tipo_computadora")
public class TipoComputadora {
	
	@Id
	@Column(name = "id_computadora")
	@GeneratedValue(strategy = GenerationType.IDENTITY)//protocolo	
	private Integer id;
	@Column(name = "tipo_equipo", length = 50)
	private String tipo;
	@Column(name = "tipo_fabricante", length = 50)
	private String fabricante;
	@Column(name = "tipo_clave", length = 50)
	private String clave;
	@Column(name = "tipo_existencias", length = 50)
	private Integer existencias;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}	
	
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getFabricante() {
		return fabricante;
	}
	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public Integer getExistencias() {
		return existencias;
	}
	public void setExistencias(Integer existencias) {
		this.existencias = existencias;
	}
	@Override
	public String toString() {
		return "[id=" + id + ", tipo=" + tipo + ", fabricante=" + fabricante + ", clave=" + clave
				+ ", existencias=" + existencias + "]";
	}
	
	
	
	
	


}
