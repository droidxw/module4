package mx.unam.tic.diplomado.agenda.modelo.entidades;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "t_precio_computadora")
@NamedQueries(
		{@NamedQuery(name = "precioEquipo", query = "SELECT c FROM Precio c WHERE c.computadora.id = :idTipoEquipo")
//		 @NamedQuery(name = "contactoById", query = "SELECT c FROM Cliente c WHERE c.id = :id"),
		})
public class Precio {
	
	@Id
	@Column(name = "id_precio")
	@GeneratedValue(strategy = GenerationType.IDENTITY)//protocolo
	private Integer id;
//	@Column(name = "clien_nombre_completo", length = 50)
	@ManyToOne(targetEntity = TipoComputadora.class, fetch = FetchType.LAZY, optional = false) //indica el tipo de clase
	@JoinColumn(name = "id_computadora", nullable = false)//indicar la columna de la relacion de B
	@Fetch(FetchMode.JOIN)
	private TipoComputadora computadora;
	
	
	@Column(name = "prec_computadora", length = 50)
	private float precio;
	@OneToMany(mappedBy = "precio", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)//subconsultas de objetos relacionados se puede con FetchType.EAGER pero no es optimo trae todas las
	private Set<Cobro> cobro;
	

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public TipoComputadora getComputadora() {
		return computadora;
	}
	public void setComputadora(TipoComputadora computadora) {
		this.computadora = computadora;
	}
	
	
	public float getPrecio() {
		return precio;
	}
	public void setPrecio(float precio) {
		this.precio = precio;
	}
	
	
	public Set<Cobro> getCobro() {
		return cobro;
	}
	public void setCobro(Set<Cobro> cobro) {
		this.cobro = cobro;
	}
	

	@Override
	public String toString() {
		return "Precio [id=" + id + ", computadora=" + computadora + "]";
	}


}
