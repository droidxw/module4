
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import mx.unam.tic.diplomado.agenda.modelo.entidades.Contacto;
import mx.unam.tic.diplomado.agenda.modelo.entidades.ContactoMedio;
import mx.unam.tic.diplomado.agenda.modelo.entidades.MedioContacto;
import mx.unam.tic.diplomado.agenda.modelo.entidades.TipoContacto;
import mx.unam.tic.diplomado.agenda.modelo.hibernate.HibernateUtil;
import mx.unam.tic.diplomado.agenda.servicios.ServiciosCatalogos;
import mx.unam.tic.diplomado.agenda.servicios.ServiciosCatalogosImpl;
import mx.unam.tic.diplomado.agenda.servicios.ServiciosContactos;
import mx.unam.tic.diplomado.agenda.servicios.ServiciosContactosImpl;

public class Agenda {

	///Inicializa servicios
	ServiciosCatalogos serviciosCatalogos = ServiciosCatalogosImpl.getInstance();
	ServiciosContactos serviciosContactos = ServiciosContactosImpl.getInstance();
	
	public void cargaMenu() {
		Scanner sn = new Scanner(System.in);
        boolean salir = false;
        int opcion; //Guardaremos la opcion del usuario
        do{
            System.out.println("1. Crear registro");
            System.out.println("2. Consultar registro");
            System.out.println("3. Consultar registro por Id");            
//            System.out.println("4. ActualizaRegistro");
            System.out.println("5. Salir");
            try {
                System.out.println("Escribe una de las opciones");
                opcion = sn.nextInt();
                switch (opcion) {
                    case 1:
                        System.out.println("Inicio de creacion registro...");
                        creaRegistro();
                        break;
                    case 2:
                        System.out.println("Cargando registros...");
                        consultaRegistros();
                        break;
                    case 3:                        
                        consultaRegistrosId();
                        break; 
//                    case 4:                        
//                    	actualizaRegistro();
//                        break;  
                    case 5:
                    	salir = true;
                        break;
                   
                    default:
                        System.out.println("Solo numeros entre 1 y 3");
                }
            } catch (InputMismatchException e) {
                System.out.println("Debes insertar un n�mero");
                sn.next();
            }
        }
        while (!salir) ;
	}
	
//	//CONSULTA todos los contactos
	public void consultaRegistros() {
		
		List<Contacto> contactos = serviciosContactos.cargaContactos();
		//Muestra info contactos
		for (Contacto contactoT : contactos) {
			System.out.println("*El contacto es:" + contactoT.getNombre());
			System.out.println("*El tipo contacto es:" + contactoT.getTipoContacto().getNombre());
		}
		
	}
	
	
	//CONSULTA POR ID
	public void consultaRegistrosId() {
		System.out.println("Introduce el valor a consultar");
		Scanner sn = new Scanner(System.in);
		int id = sn.nextInt();
		Contacto contacto1 = serviciosContactos.cargaContactoPorId(id);
		System.out.println("Id"+contacto1.getId());
		System.out.println("*El contacto es:" + contacto1.toString());
		System.out.println("*El tipo contacto es:" + contacto1.getTipoContacto().getNombre());
	}
	
	
	//Definicion nuevo contacto			
	public void  creaRegistro() {	
	
	Scanner sn = new Scanner(System.in);
	System.out.println("Introduce el nombre(s)");
	String nombre = sn.nextLine();
	System.out.println("Introduce el apellido(s)");
	String apellido = sn.nextLine();
	System.out.println("Introduce la direccion");
	String direccion = sn.nextLine();
	System.out.println("Introduce la edad");
	//int
	int edad = sn.nextInt();
	System.out.println("Introduce el id de tipo de contacto");
	//valores del 1 al 3
	int tipoCId = sn.nextInt();	
	//valores del 1 al 3
	System.out.println("Introduce el id de medio de contacto");
	int medioCId = sn.nextInt();
	
	System.out.println("Introduce el numero telefonico");
	//# telefonico
	String medioContactoTel = sn.nextLine();
	
	//***Establece valores de clase Contacto
	Contacto contacto = new Contacto();		
	contacto.setNombre(nombre);
	contacto.setApellidos(apellido);
	contacto.setDireccion(direccion);
	contacto.setEdad(edad);	
	

//	c_tipocontacto	1Fam 2Esc 3Lab		
	TipoContacto tipocontacto = serviciosCatalogos.cargaTipoContactoPorId(tipoCId);	
	contacto.setTipoContacto(tipocontacto);

//	c_mediocontacto 1casa 2trabajo 3celular
	MedioContacto medioContacto = serviciosCatalogos.cargaMedioContactoPorId(medioCId);
	
	//***Establece valores de clase ContactoMedio
	//Definicion nuevo medio
	ContactoMedio contactoMedio = new ContactoMedio();
//	atrib t_contactomedio		
	contactoMedio.setValor(medioContactoTel);
	
	contactoMedio.setMedioContacto(medioContacto);
//	atrib t_contacto		
	contactoMedio.setContacto(contacto);
	
	//**Guarda TODOS los valores establecidos de la clase ContactoMedio
	//Lista t_contacto
	Set<ContactoMedio> contactosMedios = new HashSet<ContactoMedio>();
	contactosMedios.add(contactoMedio);
	
	//**Establece TODOS los valores del atributo contactosMedio 
//	en Clase Contacto
	contacto.setContactosMedios(contactosMedios);		
	//guarda info completa en t_contacto
	serviciosContactos.guardaContacto(contacto);
	
}	
	//ACTUALIZA
	public void actualizaRegistro() {
//			Contacto contacto = new Contacto();	
//			contacto.getId();
		
		Scanner sn = new Scanner(System.in);		
		System.out.println("Introduce el id a actualizar");	
//		int id = sn.nextInt();
		
		Contacto contacto = new Contacto();
		contacto.setId(3);
		contacto.setNombre("Raul");
		contacto.setApellidos("Fernandez");
		contacto.setDireccion("Bolivar 16");
		contacto.setEdad(65);	
		
		TipoContacto tipocontacto = serviciosCatalogos.cargaTipoContactoPorId(3);	
		contacto.setTipoContacto(tipocontacto);

//		c_mediocontacto 1casa 2trabajo 3celular
		MedioContacto medioContacto = serviciosCatalogos.cargaMedioContactoPorId(2);
		
		//Definicion nuevo medio
		ContactoMedio contactoMedio = new ContactoMedio();
//		atrib t_contactomedio		
		contactoMedio.setValor("9999900000");
		
		contactoMedio.setMedioContacto(medioContacto);
//		atrib t_contacto		
		contactoMedio.setContacto(contacto);
		
		//**Guarda TODOS los valores establecidos de la clase ContactoMedio
		//Lista t_contacto
		Set<ContactoMedio> contactosMedios = new HashSet<ContactoMedio>();
		contactosMedios.add(contactoMedio);
		
		//**Establece TODOS los valores del atributo contactosMedio 
//		en Clase Contacto
		contacto.setContactosMedios(contactosMedios);		
		//guarda info completa en t_contacto
		
			serviciosContactos.actualizaContacto(contacto);	
//			System.out.println("Id"+contacto.getId());
	}

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		HibernateUtil.init();
//		///Inicializa servicios
//		ServiciosCatalogos serviciosCatalogos = ServiciosCatalogosImpl.getInstance();
//		ServiciosContactos serviciosContactos = ServiciosContactosImpl.getInstance();
		Agenda agenda = new Agenda();
		agenda.cargaMenu();
		

//*********Muestra Catalogos*********
//*******************tipo*******************		
/*		
		List<TipoContacto> tiposContacto = serviciosCatalogos.cargaTiposContacto();
		for (TipoContacto tipoContacto : tiposContacto) {
			System.out.println("-El tipo contacto es:" + tipoContacto.getNombre());
		}

		TipoContacto familiar = serviciosCatalogos.cargaTipoContactoPorId(1);
		System.out.println("-El contacto familiar===" + familiar);
		
//*******************medio*******************	
		List<MedioContacto> mediosContacto = serviciosCatalogos.cargaMediosContacto();
		for (MedioContacto medioContacto : mediosContacto) {
			System.out.println("-El medio contacto es:" + medioContacto.getNombre());
		}

		MedioContacto casa = serviciosCatalogos.cargaMedioContactoPorId(2);
		System.out.println("-El medio casa===" + casa);*/
		
//**************************************		
//*********FinMuestra Catalogos*********		

	
//ACTUALIZA
//		Contacto contacto = new Contacto();	
//		contacto.getId();
//		serviciosContactos.actualizaContacto(contacto);	
		
		

//ELIMINA
//			serviciosContactos.eliminaContacto(contacto);
//			System.out.println("*Contacto eliminado:" + contacto1.getNombre());			


	}

}
